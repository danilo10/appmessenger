import React from 'react';
import {render} from 'react-native-testing-library';
import {CircleText} from './';

describe('CircleText', () => {
  it('renders correctly', () => {
    const {toJSON} = render(<CircleText />);
    expect(toJSON).toMatchSnapshot();
  });
});
