import React from 'react';

import {Container} from './styles';
import {H1} from '../Typography';

export const Header = () => {
  return (
    <Container>
      <H1>Messages</H1>
    </Container>
  );
};
