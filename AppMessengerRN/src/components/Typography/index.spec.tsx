import React from 'react';
import {render} from 'react-native-testing-library';
import {H1, H2, Paragraph, ParagraphBold, ParagraphLight} from './';

describe('Typography', () => {
  describe('H1', () => {
    it('renders correctly', () => {
      const {toJSON} = render(<H1 />);
      expect(toJSON).toMatchSnapshot();
    });
  });

  describe('H2', () => {
    it('renders correctly', () => {
      const {toJSON} = render(<H2 />);
      expect(toJSON).toMatchSnapshot();
    });
  });

  describe('Paragraph', () => {
    it('renders correctly', () => {
      const {toJSON} = render(<Paragraph />);
      expect(toJSON).toMatchSnapshot();
    });
  });

  describe('ParagraphBold', () => {
    it('renders correctly', () => {
      const {toJSON} = render(<ParagraphBold />);
      expect(toJSON).toMatchSnapshot();
    });
  });

  describe('ParagraphLight', () => {
    it('renders correctly', () => {
      const {toJSON} = render(<ParagraphLight />);
      expect(toJSON).toMatchSnapshot();
    });
  });
});
