import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  margin-horizontal: 20px;
  margin-vertical: 10px;
`;

export const TitleContainer = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 10px;
`;

export const DetailContainer = styled.View`
  flex-direction: row;
`;

export const CircleTextContainer = styled.View`
  width: 14%;
  justify-content: center;
  align-items: flex-end;
`;
