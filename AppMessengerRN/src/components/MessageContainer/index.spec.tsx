import {render} from 'react-native-testing-library';
import {navigationMock} from '../../../__mocks__';
import {MessageContainer} from './';

describe('MessageContainer', () => {
  it('renders correctly', () => {
    const {toJSON} = render(
      MessageContainer({item: {}, index: 1}, true, navigationMock),
    );
    expect(toJSON).toMatchSnapshot();
  });
});
