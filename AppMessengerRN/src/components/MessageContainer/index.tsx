import React from 'react';
import moment from 'moment';

import {
  Container,
  TitleContainer,
  DetailContainer,
  CircleTextContainer,
} from './styles';
import {H2, Paragraph, ParagraphLight} from '../Typography';
import {Skeleton} from '../Placeholder';
import {Divider} from '../Divider';
import {CircleText} from '../CircleText';
import {IMessageContainer} from './interface';
import {IReactNavigation} from '../../interfaces';

export const MessageContainer = (
  {item, index}: IMessageContainer,
  isLoading: boolean,
  {navigation}: IReactNavigation,
) => {
  const CircleReadMessage = () => {
    return !item.read ? (
      <CircleTextContainer>
        <CircleText />
      </CircleTextContainer>
    ) : null;
  };

  return (
    <Container
      key={index}
      onPress={() => {
        item.read = true;
        navigation.navigate('detail', {item});
      }}>
      <Skeleton isLoading={!isLoading}>
        <TitleContainer>
          <H2 style={{width: '50%'}} numberOfLines={1} ellipsizeMode="tail">
            {item.subject}
          </H2>
          <ParagraphLight style={{color: !item.read ? '#CD5C5C' : '#69696A'}}>
            {moment(new Date(item.timestamp * 1000)).fromNow()}
          </ParagraphLight>
        </TitleContainer>
        <DetailContainer>
          <Paragraph
            style={{width: '85%'}}
            numberOfLines={2}
            ellipsizeMode="tail">
            {item.detail}
          </Paragraph>
          <CircleReadMessage />
        </DetailContainer>
      </Skeleton>
      <Divider />
    </Container>
  );
};
