import React from 'react';
import {render} from 'react-native-testing-library';
import {Divider} from './';

describe('Divider', () => {
  it('renders correctly', () => {
    const {toJSON} = render(<Divider />);
    expect(toJSON).toMatchSnapshot();
  });
});
