import styled from 'styled-components/native';

export const Divider = styled.View`
  margin-vertical: 10px;
  border-color: #c3c4c4;
  border-width: 0.5px;
  width: 100%;
`;
