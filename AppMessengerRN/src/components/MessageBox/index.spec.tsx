import React from 'react';
import {render} from 'react-native-testing-library';
import {MessageBox} from './';

describe('MessageBox', () => {
  it('renders correctly', () => {
    const {toJSON} = render(<MessageBox message="message" />);
    expect(toJSON).toMatchSnapshot();
  });
});
