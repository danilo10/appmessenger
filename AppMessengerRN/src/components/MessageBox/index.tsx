import React from 'react';

import {Paragraph} from '../Typography';
import {IMessageBox} from './interface';
import {Container} from './styles';

export const MessageBox = ({message}: IMessageBox) => {
  return (
    <Container>
      <Paragraph style={{color: '#f4f5f6'}}>{message}</Paragraph>
    </Container>
  );
};
