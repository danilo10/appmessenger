import {NavigationProp, RouteProp} from '@react-navigation/native';

export interface IObjectDefault {
  [name: string]: any;
}

export interface IReactNavigation {
  navigation: NavigationProp<IObjectDefault>;
}

export interface IReactNavigationRoute {
  route: RouteProp<IObjectDefault, any>;
}
