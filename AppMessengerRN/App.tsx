import React from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {RootNavigator} from './src/navigation/RootNavigation';

const App = () => (
  <PaperProvider>
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  </PaperProvider>
);

export default App;
